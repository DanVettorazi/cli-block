<h1 align="center"><strong>Library to create Blocks via code.</strong></h1>

<br />

<div align="center"><img src="imgs/top.png" /></div>

<div align="center"><strong>🚀 Create Spark Projects, without the burden of using Spark.</strong></div>

## Features

- **Scalable:** Each Patch or Material is a unique file. So, future Patches can be added here.
- **versatility**: All actions of the library (Create node, Connect nodes, Create Material, etc) can be bound with other applications, for example API's or other tools.
- **Examples ready to go:** The tool come with examples of how create patches, do connections and create Materials.
- **No configuration overhead**: The Application uses Node.js. After typing: 'npm install' you are good to go. 

## Requirements

This tool works on Spark AR r.83 and up
<div align="center"><img src="imgs/versionspark.png" /></div>
You need to have node.js and npm/yarn installed.
Tests made on OSX.

## Getting started

```
# 1. Install:
git clone git@gitlab.com:DanVettorazi/cli-block.git
cd cli-block

# 2. Run yarn install or npm install

# 3. have fun making Spark without Spark!
code .

# 4. Blocks are exported to the folder Exports
cd Exports
```

## Documentation

### How to use?
<div align="center"><img src="imgs/image.gif" /></div>

* `let MyPatch = new Animation('My awesome animation', 5);` - Instantiate an Animation Patch called 'My awesome animation' of 5 seconds.
* `let MyPatch = new Progress('My Progress', 0,14);` - Instantiate an Progress Patch called 'My Progress' starting from 0 to 14.
* `let MyMaterial = new Material('Material one','Flat',100,0,150);` - Create a Flat Material called 'Material one'. R:100,G:0,B:150.
* `Connect(fromPatch, fromPatch.Completed, toPatch, toPatch.Play);` - Instantiate a connection between two Patches.

## Contributing

Spark has hundreds of Patches and objects. All of them can, theorically, be used in this Library.
But for now just 4 connections are made.
```sh
TODO:
-Unpack
-Pack
-Material
-Delay
-OptionPicker
-Conditionals
```
```sh
TODO:
- Separate main file
-add Templates
-Add logic for the position of the Patches
-Little Refactoring
-Create a System to double check logic on the Hash generated.
-Module to import saved Blocks from Spark to save as Templates in the tool.
```
## Author

Daniel Vettorazi
