// require modules
var archiver = require('archiver');
var fs = require('fs');
var JsonTemplate = require('./Templates/TemplateBlock');
Object.assign(JsonTemplate.children[11].children[0].children[0].children[1], { children: [] })
//Patches Session:
const PTX_Anim = require('./Templates/AnimationPatch');
const PTX_Prog = require('./Templates/ProgressPatch');

const PTX_Pack = require('./Templates/PackPatch');
const PTX_Unpk = require('./Templates/UnpackPatch');
const PTX_Dela = require('./Templates/DelayPatch.json');
const PTX_Opti = require('./Templates/OptionPickerPatch.json');
const PTX_Valu = require('./Templates/ValuePatch.json');

//ValuesMap Session:
let ConnectionTemplate = require('./Templates/TemplateConnection')
//TODO: Create templates/boilerplates.

//zipper:
// var output = fs.createWriteStream(__dirname + '/Exports/'+BlockName+'.arblock');
var archive = archiver('zip', {
    zlib: { level: 9 }
});
archive.on('warning', function (err) {
    if (err.code === 'ENOENT') {
    } else throw err;
});
archive.on('error', function (err) {
    throw err;
});

const CreateHash = (segments) => {
    let random_string = '';
    let dash = '-';
    let random_ascii;
    for (let i = 0; i < segments.length; i++) {
        const element = segments[i];
        for (let i = 0; i < element; i++) {
            random_ascii = Math.floor((Math.random() * 25) + 97); //in ascii a to z is 97 to 122.
            if (Math.floor(Math.random() * 1.5)) {//0 or 1 | 50% in being a letter or a number.
                random_string += String.fromCharCode(random_ascii)
            } else {
                random_string += Math.floor(Math.random() * 9);
            }
        }
        if (i < segments.length - 1) {
            random_string += dash;
        }
    }
    return random_string
}


let BlockHash = CreateHash([8, 4, 4, 4, 12]);//gives you: '21366975-eaa5-4230-a88b-fd5afbbb0f28'//'cd6b7fca-805e-4474-8ab4-a815b6ac2b9a'// 


const GenerateBlock = (BlockName) => {
    var jsonData = JSON.stringify(JsonTemplate);
    //just save the JsonTemplate object as a new json file and zip it as arblock.
    fs.writeFile("main.json", jsonData, function (err) {
        if (err) {
            console.log(err);
        }
    });
    archive.pipe(fs.createWriteStream(__dirname + '/Exports/'+BlockName+'.arblock'));
    archive.file('main.json', { name: 'main.json' });
    archive.finalize();
}

const ValueMap = (Port, Value) => {
    //add values from the created patch to the valueMap Object.
    let VMap = JSON.parse(JSON.stringify(ConnectionTemplate[0].valuesMap));
    let PortModel = { Type: 0, value: Value };
    let templateValue = {}
    templateValue.PortModel = PortModel;
    let snippet = { [Port]: { type: 0, value: Value } }
    Object.assign(VMap, snippet)
    Object.assign(JsonTemplate.children[11].children[0].valuesMap, VMap)
}

const RearrangePatchs = (patch1, patch2, patch3) => {
    //after connected, take position of all patches and align in a row.
}


//patches variables:
let PatchOrigin = { "x": -100, "y": -100 };//todo: make logic for position
let PortIncrement = 2;

class Animation {
    constructor(Name, Duration) {
        this.Name = Name;
        this.InputDuration = Duration;
        this.SubHash = 2045;
        const PortHash = (children) => this.SubHash + 420 + (PortIncrement * children) + '-' + BlockHash;
        //Patch Hash:
        this.patchModel = "patch_model:" + this.SubHash + '-' + BlockHash;
        //Patch Input Ports:
        this.Play = "port_model:" + PortHash(0);
        this.Reverse = "port_model:" + PortHash(1);
        this.Reset = "port_model:" + PortHash(2);
        this.Duration = "port_model:" + PortHash(3);
        //Patch Output Ports:
        this.Progress = "port_model:" + PortHash(4);
        this.Completed = "port_model:" + PortHash(5);
        //MetaDataModel:
        this.Position = [PatchOrigin.x, PatchOrigin.y];
        PatchOrigin.x += 200;
    }
    patch() {
        //load Animation Patch Template:
        var stringified = JSON.stringify(PTX_Anim);
        let snippet;
        let minHash = 1440;
        let maxHash = 1950;
        let patchHash = CreateHash([8, 4, 4, 4, 12]);
        const DefinePorts = () => {
            //this function loads the template of the Patch and give a new seed for all ports.
            let j = 1;
            let ModuleHash = Math.floor(Math.random() * (1000 - 1500) + 1500);
            for (let index = minHash; index < maxHash; index++) {
                stringified = stringified.replace(new RegExp(index, "g"), ModuleHash + (j * PortIncrement));//increments in 2.
                j++;
            }
            stringified = stringified.replace(/cd6b7fca-805e-4474-8ab4-a815b6ac2b9a/g, patchHash);
            snippet = JSON.parse(stringified);
        }
        DefinePorts()
        //setting name:
        snippet.name = this.Name;
        this.patchModel = snippet.identifier;
        //Input ports:
        this.Duration = snippet.children[2].children[3].identifier;
        this.Reset = snippet.children[2].children[2].identifier;
        this.Reverse = snippet.children[2].children[1].identifier;
        this.Play = snippet.children[2].children[0].identifier;
        //Output ports:
        this.Progress = snippet.children[3].children[0].identifier;
        this.Completed = snippet.children[3].children[1].identifier;
        //Setting Position:
        snippet.children[4].position = this.Position;
        //Pushing this patch to the main block:
        JsonTemplate.children[11].children[0].children[0].children[0].children.push(snippet);
        //adding custom values of this patch to the scene:
        ValueMap(this.Duration, this.InputDuration)
        return snippet;
    }
}

class Progress {
    constructor(Name, Start, End) {
        this.Name = Name;
        this.Start = Start;
        this.End = End;
        //Patch Hash:
        this.patchModel = "patch_model:";
        //Patch Input Ports:
        this.Value = "port_model:";
        this.StartValue = "port_model:";
        this.EndValue = "port_model:";
        //Patch Output Ports:
        this.Progress = "port_model:";
        //MetaDataModel:
        this.Position = [PatchOrigin.x, PatchOrigin.y];
        PatchOrigin.x += 200;
    }
    patch() {
        //load Progress Patch Template:
        var stringified = JSON.stringify(PTX_Prog);
        let snippet;
        let minHash = 2500;
        let maxHash = 2700;
        let patchHash = CreateHash([8, 4, 4, 4, 12]);
        const DefinePorts = () => {
            //this function loads the template of the Patch and give a new seed for all ports.
            let j = 1;
            let ModuleHash = Math.floor(Math.random() * (1000 - 1500) + 1500);
            for (let index = minHash; index < maxHash; index++) {
                stringified = stringified.replace(new RegExp(index, "g"), ModuleHash + (j * PortIncrement));//increments in 2.
                j++;
            }
            stringified = stringified.replace(/cd6b7fca-805e-4474-8ab4-a815b6ac2b9a/g, patchHash);
            snippet = JSON.parse(stringified);
        }
        DefinePorts()
        //setting name:
        snippet.name = this.Name;
        this.patchModel = snippet.identifier;
        //Input ports:
        this.Value = snippet.children[2].children[0].identifier;
        this.StartValue = snippet.children[2].children[1].identifier;
        this.EndValue = snippet.children[2].children[2].identifier;
        //Output ports:
        this.Progress = snippet.children[3].children[0].identifier;
        //Setting Position:
        snippet.children[4].position = this.Position;
        //Pushing this patch to the main block:
        JsonTemplate.children[11].children[0].children[0].children[0].children.push(snippet);
        //adding custom values of this patch to the scene:
        ValueMap(this.StartValue, this.Start);
        ValueMap(this.EndValue, this.End);

        return snippet;
    }
}

class Delay {
    constructor(Name, Duration) {
        this.Name = Name;
        this.Duration = Duration;
        //Patch Hash:
        this.patchModel = "patch_model:";
        //Patch Input Ports:
        this.Input = "port_model:";
        //Patch Input Ports:
        this.Output = "port_model:";
        //MetaDataModel:
        this.Position = [PatchOrigin.x, PatchOrigin.y];
        PatchOrigin.x += 200;
    }
    patch() {
        //load Progress Patch Template:
        var stringified = JSON.stringify(PTX_Dela);
        let snippet;
        let minHash = 6200; //OK
        let maxHash = 6400; //OK
        let patchHash = CreateHash([8, 4, 4, 4, 12]);
        const DefinePorts = () => {
            //this function loads the template of the Patch and give a new seed for all ports.
            let j = 1;
            let ModuleHash = Math.floor(Math.random() * (1000 - 1500) + 1500);
            for (let index = minHash; index < maxHash; index++) {
                stringified = stringified.replace(new RegExp(index, "g"), ModuleHash + (j * PortIncrement));//increments in 2.
                j++;
            }
            stringified = stringified.replace(/cd6b7fca-805e-4474-8ab4-a815b6ac2b9a/g, patchHash);
            snippet = JSON.parse(stringified);
        }
        DefinePorts()
        //setting name:
        snippet.name = this.Name;
        this.patchModel = snippet.identifier;
        //Input ports:
        this.Input = snippet.children[2].children[0].identifier;
        this.DurationPort = snippet.children[2].children[0].identifier;
        //Output ports:
        this.Output = snippet.children[3].children[0].identifier;
        //Setting Position:
        snippet.children[4].position = this.Position;
        //Pushing this patch to the main block:
        JsonTemplate.children[11].children[0].children[0].children[0].children.push(snippet);
        //adding custom values of this patch to the scene:
        ValueMap(this.DurationPort, this.Duration);
        return snippet;
    }
}

class Unpack {
    constructor(Name, Type) {
        this.Name = Name;
        this.Type = Type;
        //Patch Hash:
        this.patchModel = "patch_model:";
        //Patch Input Ports:
        this.Input = "port_model:";
        //Patch Output Ports:
        this.Vec1 = "port_model:";
        this.Vec2 = "port_model:";
        this.Vec3 = "port_model:";
        this.Vec4 = "port_model:";
        //MetaDataModel:
        this.Position = [PatchOrigin.x, PatchOrigin.y];
        PatchOrigin.x += 200;
    }
    patch() {
        var stringified = JSON.stringify(PTX_Unpk);
        let snippet;
        let minHash = 3000; //OK
        let maxHash = 3600; //OK
        let patchHash = CreateHash([8, 4, 4, 4, 12]);
        const DefinePorts = () => {
            let j = 1;
            let ModuleHash = Math.floor(Math.random() * (1000 - 1500) + 1500);
            for (let index = minHash; index < maxHash; index++) {
                stringified = stringified.replace(new RegExp(index, "g"), ModuleHash + (j * PortIncrement));//increments in 2.
                j++;
            }
            stringified = stringified.replace(/cd6b7fca-805e-4474-8ab4-a815b6ac2b9a/g, patchHash);
            snippet = JSON.parse(stringified);
        }
        DefinePorts()
        //setting name:
        snippet.name = this.Name;
        this.patchModel = snippet.identifier;
        //Input ports:
        this.Input = snippet.children[2].children[0].identifier;
        //Output ports:
        this.vec1 = snippet.children[3].children[0].identifier;
        this.vec2 = snippet.children[3].children[1].identifier;
        this.vec3 = snippet.children[3].children[2].identifier;
        this.vec4 = snippet.children[3].children[3].identifier;
        //Setting Position:
        snippet.children[4].position = this.Position;
        //Pushing this patch to the main block:
        JsonTemplate.children[11].children[0].children[0].children[0].children.push(snippet);
        //adding custom values of this patch to the scene:
        //ValueMap(this.Input, this.Start);// todo: Check what happens on the valuemap when something is connected
        return snippet;
    }
}


class OptionPicker {
    constructor(Name, opt1, opt2, opt3, opt4, opt5, Type) {
        this.Name = Name;
        
        this.End = End;
        //Patch Hash:
        this.patchModel = "patch_model:";
        //Input:
        this.Input = "port_model:";
        this.Opt1 = "port_model:";
        this.Opt2 = "port_model:";
        this.Opt3 = "port_model:";
        this.Opt4 = "port_model:";
        //Patch Output Ports:
        this.Output = "port_model:";
        //MetaDataModel:
        this.Position = [PatchOrigin.x, PatchOrigin.y];
        PatchOrigin.x += 200;
    }
    patch() {
        //load Progress Patch Template:
        var stringified = JSON.stringify(PTX_Opti);
        let snippet;
        let minHash = 8200; //ok
        let maxHash = 8500; //ok
        let patchHash = CreateHash([8, 4, 4, 4, 12]);
        const DefinePorts = () => {
            //this function loads the template of the Patch and give a new seed for all ports.
            let j = 1;
            let ModuleHash = Math.floor(Math.random() * (1000 - 1500) + 1500);
            for (let index = minHash; index < maxHash; index++) {
                stringified = stringified.replace(new RegExp(index, "g"), ModuleHash + (j * PortIncrement));//increments in 2.
                j++;
            }
            stringified = stringified.replace(/cd6b7fca-805e-4474-8ab4-a815b6ac2b9a/g, patchHash);
            snippet = JSON.parse(stringified);
        }
        DefinePorts()
        //setting name:
        snippet.name = this.Name;
        this.patchModel = snippet.identifier;
        //Input ports:
        this.Input = snippet.children[2].children[0].identifier;
        this.Opt1 = snippet.children[2].children[1].identifier;
        this.Opt2 = snippet.children[2].children[2].identifier;
        this.Opt3 = snippet.children[2].children[3].identifier;
        this.Opt4 = snippet.children[2].children[4].identifier;
        //Output ports:
        this.Output = snippet.children[3].children[0].identifier;
        //Setting Position:
        snippet.children[4].position = this.Position;
        //Pushing this patch to the main block:
        JsonTemplate.children[11].children[0].children[0].children[0].children.push(snippet);
        //adding custom values of this patch to the scene:
        // ValueMap(this.StartValue, this.Start);//todo: check this patch behaviour on the ValueMap session
        return snippet;
    }
}

//todo: do a diff. check outside of this children to check if nothing change when you choose vec2 or vec3 instead of vec4.
class Pack {
    constructor(Name, Type) {
        this.Name = Name;
        this.Type = Type;
        //Patch Hash:
        this.patchModel = "patch_model:";
        //Patch Input Ports:
        this.Vec1 = "port_model:";
        this.Vec2 = "port_model:";
        this.vec3 = "port_model:";
        this.vec4 = "port_model:";
        //Patch Output Ports:
        this.output = "port_model:";
        //MetaDataModel:
        this.Position = [PatchOrigin.x, PatchOrigin.y];
        PatchOrigin.x += 200;
    }
    patch() {
        //load Progress Patch Template:
        var stringified = JSON.stringify(PTX_Pack);
        let snippet;
        let minHash = 5000; //ok
        let maxHash = 5600; //ok
        let patchHash = CreateHash([8, 4, 4, 4, 12]);
        const DefinePorts = () => {
            //this function loads the template of the Patch and give a new seed for all ports.
            let j = 1;
            let ModuleHash = Math.floor(Math.random() * (1000 - 1500) + 1500);
            for (let index = minHash; index < maxHash; index++) {
                stringified = stringified.replace(new RegExp(index, "g"), ModuleHash + (j * PortIncrement));//increments in 2.
                j++;
            }
            stringified = stringified.replace(/f70d8f4c-30da-47a2-896a-82ccdab9654d/g, patchHash);
            snippet = JSON.parse(stringified);
        }
        DefinePorts()
        //setting name:
        snippet.name = this.Name;
        this.patchModel = snippet.identifier;
        //Input ports:
        this.Vec1 = snippet.children[2].children[0].identifier;//port 1
        this.Vec2 = snippet.children[2].children[1].identifier;//port 2
        this.Vec3 = snippet.children[2].children[2].identifier;//port 3
        this.Vec4 = snippet.children[2].children[3].identifier;//port 4
        //Output ports:
        this.Output= snippet.children[3].children[0].identifier;//output port
        //Setting Position:
        snippet.children[4].position = this.Position;
        //Pushing this patch to the main block:
        JsonTemplate.children[11].children[0].children[0].children[0].children.push(snippet);
        //adding custom values of this patch to the scene:
        // ValueMap(this.Vec1, this.Start);//todo: check this
        return snippet;
    }
}



const Connect = (FromPatch, FromPort, ToPatch, ToPort) => {
    let snippet = {
        "fromPatch": FromPatch.patchModel,
        "fromPort": FromPort,
        "identifier": "connection_model:" + Math.floor(Math.random() * (1000 - 1500) + 1500) + '-' + BlockHash,
        "modelName": "connection_model",
        "toPatch": ToPatch.patchModel,
        "toPort": ToPort
    }
    JsonTemplate.children[11].children[0].children[0].children[1].children.push(snippet)
}


const SetMaterial = (FromPatch, FromPort, ToMaterial) => {
    let snippet = {
        "fromPatch": FromPatch.patchModel,
        "fromPort": FromPort,
        "identifier": "connection_model:" + Math.floor(Math.random() * (1000 - 1500) + 1500) + '-' + BlockHash,
        "modelName": "connection_model",
        "toPatch": ToPatch.patchModel,
        "toPort": ToPort//TODO: SEE THE STRUCTURE OF THE INPUT PORT OF A MATERIAL
    }
    JsonTemplate.children[11].children[0].children[0].children[1].children.push(snippet)
}



//as reference:
// console.log(animapatch.patch())//if you want to see what's the patch you've just created
// console.log(animapatch.patch()[2])//if you want to see some child element in the patch you've just created
// console.log(animapatch.Duration)//if you want to see the setting of some patch object.



// // example:
let animationPatch1 = new Animation('My awesome animation', 5);
animationPatch1.patch(); //add the patch into the block.

let animationPatch2 = new Animation('another pretty cool animation', 3);
animationPatch2.patch();

let ProgressPatch = new Progress('Progress!', 0, 100);
ProgressPatch.patch();

// //let's connect this nodes:
Connect(animationPatch1, animationPatch1.Completed, animationPatch2, animationPatch2.Play);
//one more...
Connect(animationPatch2, animationPatch2.Progress, ProgressPatch, ProgressPatch.Value);

GenerateBlock('oi');